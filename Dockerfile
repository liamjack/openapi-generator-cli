FROM openapitools/openapi-generator-cli:latest

RUN apk add --no-cache openssh-client git gettext

COPY openapi-generator-cli.jar /opt/openapi-generator/modules/openapi-generator-cli/target/openapi-generator-cli.jar 
